﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using volvo.truck.api.Data;

#nullable disable

namespace volvo.truck.api.Migrations
{
    [DbContext(typeof(LocalDBContext))]
    partial class LocalDBContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "6.0.3")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder, 1L, 1);

            modelBuilder.Entity("volvo.truck.api.Model.Definitions.ManufacturingYear", b =>
                {
                    b.Property<long>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<long>("ID"), 1L, 1);

                    b.Property<int>("Year")
                        .HasColumnType("int");

                    b.HasKey("ID");

                    b.ToTable("ManufacturingYears");
                });

            modelBuilder.Entity("volvo.truck.api.Model.Definitions.ModelYear", b =>
                {
                    b.Property<long>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<long>("ID"), 1L, 1);

                    b.Property<int>("Year")
                        .HasColumnType("int");

                    b.HasKey("ID");

                    b.ToTable("ModelYears");
                });

            modelBuilder.Entity("volvo.truck.api.Model.Definitions.VolvoModel", b =>
                {
                    b.Property<long>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<long>("ID"), 1L, 1);

                    b.Property<bool>("Enabled")
                        .HasColumnType("bit");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("ID");

                    b.ToTable("VolvoModels");
                });

            modelBuilder.Entity("volvo.truck.api.Model.Vehicles.Truck", b =>
                {
                    b.Property<long>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<long>("ID"), 1L, 1);

                    b.Property<bool>("Enabled")
                        .HasColumnType("bit");

                    b.Property<long>("ManufacturingYearID")
                        .HasColumnType("bigint");

                    b.Property<long>("ModelYearID")
                        .HasColumnType("bigint");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<long>("VolvoModelID")
                        .HasColumnType("bigint");

                    b.HasKey("ID");

                    b.HasIndex("ManufacturingYearID");

                    b.HasIndex("ModelYearID");

                    b.HasIndex("VolvoModelID");

                    b.ToTable("Trucks");
                });

            modelBuilder.Entity("volvo.truck.api.Model.Vehicles.Truck", b =>
                {
                    b.HasOne("volvo.truck.api.Model.Definitions.ManufacturingYear", "ManufacturingYear")
                        .WithMany()
                        .HasForeignKey("ManufacturingYearID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("volvo.truck.api.Model.Definitions.ModelYear", "ModelYear")
                        .WithMany()
                        .HasForeignKey("ModelYearID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("volvo.truck.api.Model.Definitions.VolvoModel", "VolvoModel")
                        .WithMany()
                        .HasForeignKey("VolvoModelID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("ManufacturingYear");

                    b.Navigation("ModelYear");

                    b.Navigation("VolvoModel");
                });
#pragma warning restore 612, 618
        }
    }
}
