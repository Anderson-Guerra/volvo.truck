﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
namespace volvo.truck.api.Migrations
{
    public partial class Volvo_INI : Migration
    {

        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ManufacturingYears",
                columns: table => new
                {
                    ID = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Year = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ManufacturingYears", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "ModelYears",
                columns: table => new
                {
                    ID = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Year = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ModelYears", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "VolvoModels",
                columns: table => new
                {
                    ID = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Enabled = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VolvoModels", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Trucks",
                columns: table => new
                {
                    ID = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    VolvoModelID = table.Column<long>(type: "bigint", nullable: false),
                    ModelYearID = table.Column<long>(type: "bigint", nullable: false),
                    ManufacturingYearID = table.Column<long>(type: "bigint", nullable: false),
                    Enabled = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trucks", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Trucks_ManufacturingYears_ManufacturingYearID",
                        column: x => x.ManufacturingYearID,
                        principalTable: "ManufacturingYears",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Trucks_ModelYears_ModelYearID",
                        column: x => x.ModelYearID,
                        principalTable: "ModelYears",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Trucks_VolvoModels_VolvoModelID",
                        column: x => x.VolvoModelID,
                        principalTable: "VolvoModels",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Trucks_ManufacturingYearID",
                table: "Trucks",
                column: "ManufacturingYearID");

            migrationBuilder.CreateIndex(
                name: "IX_Trucks_ModelYearID",
                table: "Trucks",
                column: "ModelYearID");

            migrationBuilder.CreateIndex(
                name: "IX_Trucks_VolvoModelID",
                table: "Trucks",
                column: "VolvoModelID");

            #region First insert
            migrationBuilder.Sql("INSERT INTO VolvoModels VALUES ('No model selected', 0);");

            migrationBuilder.Sql("INSERT INTO VolvoModels VALUES ('VNL 300 Daycab', 1);");
            migrationBuilder.Sql("INSERT INTO VolvoModels VALUES ('VNL 400 Flat-roof', 1);");
            migrationBuilder.Sql("INSERT INTO VolvoModels VALUES ('VNL 740', 1);");
            migrationBuilder.Sql("INSERT INTO VolvoModels VALUES ('VNL 760', 1);");
            migrationBuilder.Sql("INSERT INTO VolvoModels VALUES ('VNL 860', 1);");

            migrationBuilder.Sql("INSERT INTO VolvoModels VALUES ('VNR 300', 1);");
            migrationBuilder.Sql("INSERT INTO VolvoModels VALUES ('VNR 400', 1);");
            migrationBuilder.Sql("INSERT INTO VolvoModels VALUES ('VNR 640', 1);");

            migrationBuilder.Sql("INSERT INTO VolvoModels VALUES ('VNX Daycab', 1);");
            migrationBuilder.Sql("INSERT INTO VolvoModels VALUES ('VNX 400 Flat-Roof', 1);");
            migrationBuilder.Sql("INSERT INTO VolvoModels VALUES ('VNX 740 Mid-Roof', 1);");

            migrationBuilder.Sql("INSERT INTO VolvoModels VALUES ('VHD 300 AF – Axles Foward', 1);");
            migrationBuilder.Sql("INSERT INTO VolvoModels VALUES ('VHD 300 AB – Axle Back', 1);");
            migrationBuilder.Sql("INSERT INTO VolvoModels VALUES ('VHD 400 – Sleeper Cab', 1);");

            migrationBuilder.Sql("INSERT INTO VolvoModels VALUES ('VAH 300 Daycab', 1);");
            migrationBuilder.Sql("INSERT INTO VolvoModels VALUES ('VAH 400 Flatroof', 1);");
            migrationBuilder.Sql("INSERT INTO VolvoModels VALUES ('VAH 600 Flatroof Sleeper', 1);");

            migrationBuilder.Sql("INSERT INTO VolvoModels VALUES ('FH', 1);");
            migrationBuilder.Sql("INSERT INTO VolvoModels VALUES ('FH LNG', 1);");
            migrationBuilder.Sql("INSERT INTO VolvoModels VALUES ('FH12', 1);");
            migrationBuilder.Sql("INSERT INTO VolvoModels VALUES ('FH13', 1);");
            migrationBuilder.Sql("INSERT INTO VolvoModels VALUES ('FH16', 1);");
            migrationBuilder.Sql("INSERT INTO VolvoModels VALUES ('FH400', 1);");
            migrationBuilder.Sql("INSERT INTO VolvoModels VALUES ('FH420', 1);");
            migrationBuilder.Sql("INSERT INTO VolvoModels VALUES ('FH440', 1);");
            migrationBuilder.Sql("INSERT INTO VolvoModels VALUES ('FH460', 1);");
            migrationBuilder.Sql("INSERT INTO VolvoModels VALUES ('FH480', 1);");
            migrationBuilder.Sql("INSERT INTO VolvoModels VALUES ('FH500', 1);");
            migrationBuilder.Sql("INSERT INTO VolvoModels VALUES ('FH520', 1);");
            migrationBuilder.Sql("INSERT INTO VolvoModels VALUES ('FH540', 1);");
            migrationBuilder.Sql("INSERT INTO VolvoModels VALUES ('FH580', 1);");
            migrationBuilder.Sql("INSERT INTO VolvoModels VALUES ('FH660', 1);");

            migrationBuilder.Sql("INSERT INTO VolvoModels VALUES ('FM7', 1);");
            migrationBuilder.Sql("INSERT INTO VolvoModels VALUES ('FM10', 1);");
            migrationBuilder.Sql("INSERT INTO VolvoModels VALUES ('FM12', 1);");

            int iniYear = 1996;
            do
            {
                if (iniYear.Equals(1996))
                    migrationBuilder.Sql(String.Format("INSERT INTO ModelYears VALUES ({0});", iniYear));

                migrationBuilder.Sql(String.Format("INSERT INTO ManufacturingYears VALUES ({0});", iniYear));
                migrationBuilder.Sql(String.Format("INSERT INTO ModelYears VALUES ({0});", (iniYear + 1)));
                iniYear++;
            } while (iniYear < DateTime.Now.AddYears(1).Year);


            int initialTruck = 1, maxTrucks = 50;
            do
            {
                int manufatureYear = new Random().Next(1, 26);

                migrationBuilder.Sql(
                    String.Format(
                        "INSERT INTO trucks (Name, VolvoModelID, ManufacturingYearID, ModelYearID, Enabled) VALUES ('{0}', {1}, {2}, {3}, {4});"
                    , String.Concat("Example truck ", initialTruck.ToString("D5"))
                    , new Random().Next(2, 37)
                    , manufatureYear
                    , new Random().Next(0, 2) == 0 ? manufatureYear : (manufatureYear + 1)
                    , new Random().Next(0, 2)
                    ));
                initialTruck++;
            } while (initialTruck <= maxTrucks);
            #endregion First insert
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Trucks");

            migrationBuilder.DropTable(
                name: "ManufacturingYears");

            migrationBuilder.DropTable(
                name: "ModelYears");

            migrationBuilder.DropTable(
                name: "VolvoModels");
        }
    }
}
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
