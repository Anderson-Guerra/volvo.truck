using Microsoft.AspNetCore.Diagnostics;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using System.Reflection;
using System.Text;
using volvo.truck.api.Data;
using volvo.truck.api.Model;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddDbContext<LocalDBContext>();
builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo
    {
        Version = "V1"
    ,   Title = "Volvo Truck Administration"
    ,   Description = "Test for Software enginer at Volvo."
    ,   Contact = new OpenApiContact
        {
            Name = "Anderson Guerra"
        ,   Email = "hello@anderson-guerra.com"
        ,   Url = new Uri("https://anderson-guerra.com")
        }
    });
    options.IncludeXmlComments(
        Path.Combine(AppContext.BaseDirectory
    ,   $"{Assembly.GetExecutingAssembly().GetName().Name}.xml")
    );
});
builder.Services.AddCors(options =>
{
    options.AddPolicy(name: "CorsPolicy", 
        policy =>
        {
            policy
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader();
        });
});

var app = builder.Build();
if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
    using (IServiceScope scope = app.Services.CreateScope())
    {        
        scope
            .ServiceProvider
            .GetRequiredService<LocalDBContext>()
            .Database
            .Migrate();

        if (!Connection.CheckLocalDB())
            throw new Exception("Please make sure that 'localDB' ConnectionStrings exists on 'appsettings.json' and can connect the DataBase related.");
    }
}
app.UseExceptionHandler(errorApp =>
{
    errorApp.Run(async context =>
    {
        context.Response.StatusCode = 400;
        context.Response.ContentType = "application/json";

        var error = context.Features.Get<IExceptionHandlerFeature>();
        if (error != null)
        {
            var message = error.Error;
            await context.Response.WriteAsync(new Error()
            {
                Code = 400,
                Message = message.Message
            }.ToString(), Encoding.UTF8);
        }
    });
});
app.UseSwagger();
app.UseSwaggerUI(options =>
{
    options.RoutePrefix = string.Empty;
    options.SwaggerEndpoint("swagger/v1/swagger.json", "V1");
});
//app.UseHttpsRedirection();
app.UseAuthorization();
app.MapControllers();
app.UseCors("CorsPolicy");
app.Run();
