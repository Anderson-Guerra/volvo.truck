﻿using Microsoft.EntityFrameworkCore;
using volvo.truck.api.Model.Vehicles;
using volvo.truck.api.Model.Definitions;

namespace volvo.truck.api.Data
{
    /// <summary>
    /// LocalDB Context.
    /// </summary>
    public class LocalDBContext : DbContext
    {
        /// <summary>
        /// Volvo Vehicles Models
        /// </summary>
        public DbSet<VolvoModel>? VolvoModels {  get; set; }
        /// <summary>
        /// Volvo Trucks
        /// </summary>
        public DbSet<Truck>? Trucks { get; set; }
        /// <summary>
        /// Volvo trucks model year
        /// </summary>
        public DbSet<ModelYear>? ModelYears { get; set; }
        /// <summary>
        /// Volvo trucks manufature year
        /// </summary>
        public DbSet<ManufacturingYear>? ManufacturingYears { get; set; }


        /// <summary>
        /// Assign connection string
        /// </summary>
        /// <param name="optionsBuilder"></param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(Connection.LocalDBConn);
        }
        /// <summary>
        /// Custom keys
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<VolvoModel>(entity => { entity.HasKey(k => new { k.ID }); });
            modelBuilder.Entity<Truck>(entity => { entity.HasKey(k => new { k.ID }); });
            modelBuilder.Entity<ModelYear>(entity => { entity.HasKey(k => new { k.ID }); });
            modelBuilder.Entity<ManufacturingYear>(entity => { entity.HasKey(k => new { k.ID }); });
        }
    }
}
