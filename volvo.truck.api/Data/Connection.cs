﻿using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;

namespace volvo.truck.api.Data
{
    /// <summary>
    /// Connection Collection
    /// </summary>
    public static class Connection
    {
        /// <summary>
        /// DefaultConnection connection string
        /// </summary>
        internal static string DefaultConn
        {
            get
            {
                return WebApplication.CreateBuilder().Configuration.GetConnectionString("DefaultConnection");
            }

        }
        /// <summary>
        /// LocalDB connection string
        /// </summary>
        internal static string LocalDBConn
        {
            get
            {
                return WebApplication.CreateBuilder().Configuration.GetConnectionString("localDB");
            }

        }
        /// <summary>
        /// Check LocalDB.
        /// </summary>
        /// <returns></returns>
        public static bool CheckLocalDB()
        {
            using (var l_oConnection = new SqlConnection(Connection.LocalDBConn))
            {
                try
                {
                    l_oConnection.Open();
                    return true;
                }
                catch (SqlException)
                {
                    return false;
                }
            }
        }
    }
}
