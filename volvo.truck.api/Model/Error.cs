﻿using Newtonsoft.Json;

namespace volvo.truck.api.Model
{
    /// <summary>
    /// Error custom handler
    /// </summary>
    public class Error
    {
        /// <summary>
        /// Error ID.
        /// </summary>
        public int Code { get; set; }
        /// <summary>
        /// Error Message.
        /// </summary>
        public string? Message { get; set; }

        /// <summary>
        /// Convert to JSON
        /// </summary>
        /// <returns>json result</returns>
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
