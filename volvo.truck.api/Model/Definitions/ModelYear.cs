﻿namespace volvo.truck.api.Model.Definitions
{
    /// <summary>
    /// Volvo vehicle model year.
    /// </summary>
    public class ModelYear
    {
        /// <summary>
        /// Year model ID.
        /// </summary>
        public long ID { get; set; }
        /// <summary>
        /// Year model name.
        /// </summary>
        public int Year { get; set; }
    }
}
