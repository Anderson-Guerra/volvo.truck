﻿namespace volvo.truck.api.Model.Definitions
{
    /// <summary>
    /// Volvo vehicle model year manufacturing.
    /// </summary>
    public class ManufacturingYear
    {
        /// <summary>
        /// Year manufacturing ID.
        /// </summary>
        public long ID { get; set; }
        /// <summary>
        /// Year manufacturing name.
        /// </summary>
        public int Year { get; set; }
    }
}
