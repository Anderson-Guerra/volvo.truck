﻿namespace volvo.truck.api.Model.Definitions
{
    /// <summary>
    /// Volvo vehicle model.
    /// </summary>
    public class VolvoModel
    {
        /// <summary>
        /// Model ID.
        /// </summary>
        public long ID { get; set; }
        /// <summary>
        /// Model name.
        /// </summary>
        public string Name { get; set; } = null!;
        /// <summary>
        /// Model flag enabled.
        /// </summary>
        public bool Enabled { get; set; } = false;
    }
}
