﻿using volvo.truck.api.Model.Definitions;

namespace volvo.truck.api.Model.Vehicles
{
    /// <summary>
    /// Trucks
    /// </summary>
    public class Truck
    {
        /// <summary>
        /// Truck ID.
        /// </summary>
        public long ID {  get; set; }
        /// <summary>
        /// Truck name.
        /// </summary>
        public string? Name { get; set; }
        /// <summary>
        /// Model ID.
        /// </summary>
        public VolvoModel VolvoModel { get; set; } = null!;
        /// <summary>
        /// Model year.
        /// </summary>
        public ModelYear ModelYear { get; set; } = null!;
        /// <summary>
        /// Truck manufacturing year.
        /// </summary>
        public ManufacturingYear ManufacturingYear { get; set; } = null!;
        /// <summary>
        /// Truck is enabled.
        /// </summary>
        public bool Enabled { get; set; }
    }
}
