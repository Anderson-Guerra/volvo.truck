using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using volvo.truck.api.Data;
using volvo.truck.api.Model.Definitions;
using volvo.truck.api.Model.Vehicles;

namespace volvo.truck.api.Controllers.Vehicles
{
    /// <summary>
    /// Volvo truck.
    /// </summary>
    [ApiController]
    [Route("truck")]
    public class TruckController : ControllerBase
    {
        [NonAction]
        private static Truck Validate(LocalDBContext dbContext, Truck eTruck)
        {
            if (eTruck.VolvoModel is null || eTruck.VolvoModel.ID.Equals(0))
                throw new Exception("Volvo model ID, required!", null);
            if (eTruck.ModelYear is null || eTruck.ModelYear.ID.Equals(0))
                throw new Exception("Model Year ID, required!", null);
            if (eTruck.ManufacturingYear is null || eTruck.ManufacturingYear.ID.Equals(0))
                throw new Exception("Manufacturing Year ID, required!", null);

            if (String.IsNullOrEmpty(eTruck.Name) || eTruck.Name.Length < 3)
                throw new Exception("Truck Name invalid, need to have 3 or more characters!", null);

            #pragma warning disable CS8601
            eTruck.VolvoModel = dbContext.VolvoModels.Where(where => where.ID.Equals(eTruck.VolvoModel.ID)).FirstOrDefault();
            eTruck.ModelYear = dbContext.ModelYears.Where(where => where.ID.Equals(eTruck.ModelYear.ID)).FirstOrDefault();
            eTruck.ManufacturingYear = dbContext.ManufacturingYears.Where(where => where.ID.Equals(eTruck.ManufacturingYear.ID)).FirstOrDefault();
                
            if (eTruck.ManufacturingYear?.Year > eTruck.ModelYear?.Year)
                throw new Exception("Manufacturing Year need to be lower or equal model year!", null);
            if (eTruck.ModelYear?.Year > (eTruck.ManufacturingYear?.Year+1))
                throw new Exception("Model year is only allowed one year in advance from manufacture date!", null);
            #pragma warning restore CS8601

            return eTruck;
        }

        /// <summary>
        /// Truck full list.
        /// </summary>
        /// <returns>Full list of trucks</returns>
        /// <remarks>
        /// Example:
        /// 
        ///     GET
        ///         /truck/get
        ///         
        /// </remarks>
        [HttpGet("get")]
        public ICollection<Truck> Get()
        {
            IEnumerable<Truck> result = this.GetEnabled().Union(this.GetDisabled());            
            return result
                .OrderByDescending(order => order.Enabled)
                .ThenByDescending(order => order.ID)
                .ToList();
        }

        /// <summary>
        /// Enabled truck full list.
        /// </summary>
        /// <returns>Full list of enabled trucks</returns>
        /// <remarks>
        /// Example:
        /// 
        ///     GET
        ///       /truck/get-enabled
        ///         
        /// </remarks>
        [HttpGet("get-enabled")]
        public ICollection<Truck> GetEnabled()
        {
            return this.Get(true, 1, int.MaxValue, "*");
        }

        /// <summary>
        /// Disabled truck full list.
        /// </summary>
        /// <returns>Full list of disabled trucks</returns>
        /// <remarks>
        /// Example:
        /// 
        ///     GET
        ///       /truck/get-disabled
        ///         
        /// </remarks>
        [HttpGet("get-disabled")]
        public ICollection<Truck> GetDisabled()
        {
            return this.Get(false, 1, int.MaxValue, "*");
        }

        /// <summary>
        /// Truck list.
        /// </summary>
        /// <param name="enabled">Enabled flag</param>        
        /// <param name="page">Page number</param>
        /// <param name="itemPerPage">Total itens per page</param>
        /// <param name="name">Truck name</param>
        /// <returns>list of trucks</returns>
        /// <remarks>
        /// Example:
        /// 
        ///     GET
        ///       /truck/get/true/1/10/%2A
        ///         
        /// </remarks>
        [HttpGet("get/{enabled:bool}/{page:int}/{itemPerPage:int}/{name?}")]
        public ICollection<Truck> Get(bool enabled = true, int page = 1, int itemPerPage = 10, string name = "*")
        {
            page = (page > 0 ? page : 1);
            itemPerPage = (itemPerPage > 0 ? itemPerPage : 10);

            using LocalDBContext dbContext = new();
            List<Truck> result = dbContext.Trucks
                .Include(include => include.VolvoModel)
                .Include(include => include.ManufacturingYear)
                .Include(include => include.ModelYear)
                .Where(where => where.Enabled.Equals(enabled))
                .Where(where => name.Equals("*") || (where.Name ?? string.Empty).Contains(name))
                .Skip(((page - 1) * itemPerPage))
                .Take(itemPerPage)
                .ToList();

            return result;
        }

        /// <summary>
        /// Truck by ID.
        /// </summary>
        /// <returns>Return a truck</returns>
        /// <remarks>
        /// Example:
        /// 
        ///     GET
        ///         /truck/get/1
        ///         
        /// </remarks>
        [HttpGet("get/{id:long}")]
        public Truck Get(long id)
        {
            using LocalDBContext dbContext = new();
            return dbContext.Trucks
                .Include(include => include.VolvoModel)
                .Include(include => include.ManufacturingYear)
                .Include(include => include.ModelYear)
                .Where(where => where.ID.Equals(id))
                .First();
        }

        /// <summary>
        /// Create new truck
        /// </summary>
        /// <param name="eTruck">Truck class to be added</param>
        /// <returns>Last inserted truck with ID</returns>
        /// <remarks>
        /// Example:
        /// 
        ///        POST:
        ///         {
        ///           "name": "Teste truck Insert API",
        ///           "volvoModel": {
        ///             "id": 2,
        ///             "name": "",
        ///             "enabled": true
        ///           },
        ///           "modelYear": {
        ///             "id": 1,
        ///             "year": 0
        ///           },
        ///           "manufacturingYear": {
        ///             "id": 1,
        ///             "year": 0
        ///           },
        ///           "enabled": true
        ///         }
        ///         
        /// </remarks>
        [HttpPost("post")]
        public Truck Post([FromBody]Truck eTruck)
        {
            using (LocalDBContext dbContext = new())
            {
                eTruck.ID = 0;
                eTruck = Validate(
                    dbContext
                ,   eTruck
                );

                dbContext.Add(eTruck);
                dbContext.SaveChanges();
            }
            return eTruck;
        }

        /// <summary>
        /// Update truck
        /// </summary>
        /// <param name="eTruck">Truck class to be updated</param>
        /// <returns>Last updated truck</returns>
        /// <remarks>
        /// Example:
        /// 
        ///        POST:
        ///         {
        ///           "id": 1,
        ///           "name": "Teste truck Update API",
        ///           "volvoModel": {
        ///             "id": 2,
        ///             "name": "",
        ///             "enabled": true
        ///           },
        ///           "modelYear": {
        ///             "id": 1,
        ///             "year": 0
        ///           },
        ///           "manufacturingYear": {
        ///             "id": 1,
        ///             "year": 0
        ///           },
        ///           "enabled": true
        ///         }
    ///         
    /// </remarks>
        [HttpPut("put")]
        public Truck Put([FromBody] Truck eTruck)
        {
            using LocalDBContext dbContext = new();
            Truck? uTruck = dbContext
                .Trucks
                .Include(include => include.VolvoModel)
                .Include(include => include.ManufacturingYear)
                .Include(include => include.ModelYear)
                .FirstOrDefault(where => where.ID.Equals(eTruck.ID));

            if (uTruck is null || uTruck.ID.Equals(0))
                throw new Exception("Truck, not found!", null);
            
            if (!eTruck.VolvoModel.ID.Equals(uTruck.VolvoModel.ID))
            {
                VolvoModel? eVolvoModel = dbContext.VolvoModels.Where(where => where.ID.Equals(eTruck.VolvoModel.ID)).FirstOrDefault();

                if (eVolvoModel is not null)
                    uTruck.VolvoModel = eVolvoModel;
            }
            if (!eTruck.ModelYear.ID.Equals(uTruck.ModelYear.ID))
            {
                ModelYear? eModelYear = dbContext.ModelYears.Where(where => where.ID.Equals(eTruck.ModelYear.ID)).FirstOrDefault();

                if (eModelYear is not null)
                    uTruck.ModelYear = eModelYear;
            }
            if (!eTruck.ManufacturingYear.ID.Equals(uTruck.ManufacturingYear.ID))
            {
                ManufacturingYear? eManufacturingYear = dbContext.ManufacturingYears.Where(where => where.ID.Equals(eTruck.ManufacturingYear.ID)).FirstOrDefault();

                if (eManufacturingYear is not null)
                    uTruck.ManufacturingYear = eManufacturingYear;
            }
            uTruck.Name = eTruck.Name;
            uTruck.Enabled = eTruck.Enabled;

            uTruck = Validate(
                dbContext
            ,   uTruck
            );

            dbContext.SaveChanges();
            return uTruck;
        }

        /// <summary>
        /// Disable truck
        /// </summary>
        /// <param name="id">ID Truck to be disabled</param>
        /// <returns>OK message</returns>
        /// <remarks>
        /// Example:
        /// 
        ///         Delete:
        ///             /truck/delete/1
        ///         
        /// </remarks>
        [HttpDelete("delete/{id:long}")]
        public OkResult Delete(long id)
        {
            using (LocalDBContext dbContext = new())
            {
                if (id <= 0)
                    throw new Exception("ID truck, required!", null);

                IEnumerable<Truck> eTruck = dbContext.Trucks.Where(where => where.ID.Equals(id));


                if (eTruck is null || !eTruck.Any() || eTruck.First().ID.Equals(0))
                    throw new Exception("ID truck not found!", null);

                foreach(Truck fTruck in eTruck)
                    fTruck.Enabled = false;
                
                dbContext.SaveChanges();
            }
            return Ok();
        }
    }
}