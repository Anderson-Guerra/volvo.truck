﻿using Microsoft.AspNetCore.Mvc;
using volvo.truck.api.Data;
using volvo.truck.api.Model.Definitions;

namespace volvo.truck.api.Controllers.Definitions
{
    /// <summary>
    /// Volvo vehicles year
    /// </summary>
    [ApiController]
    [Route("year")]
    public class YearController : Controller
    {
        /// <summary>
        /// Model year full list.
        /// </summary>
        /// <returns>Full list of active trucks</returns>
        /// <remarks>
        /// Example:
        /// 
        ///     GET
        ///      /year/get-model
        ///         
        /// </remarks>
        [HttpGet("get-model")]
        public ICollection<ModelYear> GetModel()
        {
            return this.GetModel(1, int.MaxValue);
        }

        /// <summary>
        /// Model year list.
        /// </summary>     
        /// <param name="page">Page number</param>
        /// <param name="itemPerPage">Total itens per page</param>
        /// <returns>Year list for truck model</returns>
        /// <remarks>
        /// Example:
        /// 
        ///     GET
        ///      /year/get-model/1/10
        ///
        /// </remarks>
        [HttpGet("get-model/{page:int}/{itemPerPage:int}")]
        public ICollection<ModelYear> GetModel(int page = 1, int itemPerPage = 10)
        {
            page = (page > 0 ? page : 1);
            itemPerPage = (itemPerPage > 0 ? itemPerPage : 10);

            using (LocalDBContext dbContext = new LocalDBContext())
            {
                return dbContext.ModelYears
                    .OrderByDescending(orderBy => orderBy.Year)
                    .Skip(((page - 1) * itemPerPage))
                    .Take(itemPerPage)
                    .ToList();
            }
        }

        /// <summary>
        /// Manufatured year full list.
        /// </summary>
        /// <returns>Full list of active trucks</returns>
        /// <remarks>
        /// Example:
        /// 
        ///     GET
        ///      /year/get-manufacturing
        ///         
        /// </remarks>
        [HttpGet("get-manufacturing")]
        public ICollection<ManufacturingYear> GetManufacturing()
        {
            return this.GetManufacturing(1, int.MaxValue);
        }

        /// <summary>
        /// Manufatured year list.
        /// </summary>     
        /// <param name="page">Page number</param>
        /// <param name="itemPerPage">Total itens per page</param>
        /// <returns>List of truck manufatured year</returns>
        /// <remarks>
        /// Example:
        /// 
        ///     GET 
        ///      /year/get-manufacturing/1/10
        ///      
        /// </remarks>
        [HttpGet("get-manufacturing/{page:int}/{itemPerPage:int}")]
        public ICollection<ManufacturingYear> GetManufacturing(int page = 1, int itemPerPage = 10)
        {
            page = (page > 0 ? page : 1);
            itemPerPage = (itemPerPage > 0 ? itemPerPage : 10);

            using (LocalDBContext dbContext = new LocalDBContext())
            {
                return dbContext.ManufacturingYears
                    .OrderByDescending(orderBy => orderBy.Year)
                    .Skip(((page - 1) * itemPerPage))
                    .Take(itemPerPage)
                    .ToList();
            }
        }
    }
}
