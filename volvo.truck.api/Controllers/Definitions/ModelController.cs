﻿using Microsoft.AspNetCore.Mvc;
using volvo.truck.api.Data;
using volvo.truck.api.Model.Definitions;

namespace volvo.truck.api.Controllers.Definitions
{
    /// <summary>
    /// Volvo vehicles model
    /// </summary>
    [ApiController]
    [Route("model")]
    public class ModelController : Controller
    {
        /// <summary>
        /// Truck model full list.
        /// </summary>
        /// <returns>Full list of active trucks</returns>
        /// <remarks>
        /// Example:
        /// 
        ///     GET
        ///      /model/get
        ///         
        /// </remarks>
        [HttpGet("get")]
        public ICollection<VolvoModel> Get()
        {
            return this.Get(true, 1, int.MaxValue, "*");
        }

        /// <summary>
        /// Truck model list.
        /// </summary>
        /// <param name="enabled">Enabled flag</param>        
        /// <param name="page">Page number</param>
        /// <param name="itemPerPage">Total itens per page</param>
        /// <param name="name">Model name</param>
        /// <returns>List of manufatured truck model</returns>
        /// <remarks>
        /// Example:
        /// 
        ///     GET
        ///      /model/get/true/1/10/%2A
        ///      
        /// </remarks>
        [HttpGet("get/{enabled:bool}/{page:int}/{itemPerPage:int}/{name?}")]
        public ICollection<VolvoModel> Get(bool enabled = true, int page = 1, int itemPerPage = 10, string name = "*")
        {
            page = (page > 0 ? page : 1);
            itemPerPage = (itemPerPage > 0 ? itemPerPage : 10);

            using (LocalDBContext dbContext = new LocalDBContext())
            {
                return dbContext.VolvoModels
                    .Where(where => where.Enabled.Equals(enabled))
                    .Where(where => name.Equals("*") || (where.Name??string.Empty).Contains(name))
                    .Skip(((page - 1) * itemPerPage))
                    .Take(itemPerPage)
                    .ToList();
            }
        }
    }
}
