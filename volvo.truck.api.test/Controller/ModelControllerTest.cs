﻿using System.Collections.Generic;
using System.Linq;
using volvo.truck.api.Controllers.Definitions;
using volvo.truck.api.Model.Definitions;
using Xunit;

namespace Controller;

public class ModelControllerTest
{
    /// <summary>
    /// Should return list of enabled Model.
    /// </summary>
    [Fact]
    public void ListEnabledModel()
    {
        ICollection<VolvoModel> result = new ModelController().Get();

        Assert.NotEmpty(result);
        Assert.InRange(result.Count(), 1, int.MaxValue);
        Assert.DoesNotContain(result, any => any.Enabled.Equals(false));
    }

    /// <summary>
    /// Should return a filled Model
    /// </summary>
    [Fact]
    public void ListFilteredModel()
    {
        ICollection<VolvoModel> result = new ModelController().Get(true, 1, 1, "*");

        Assert.Collection(
            result
        , collection =>
        {
            Assert.NotNull(collection.Name);
            Assert.True(collection.Enabled);
        }
      );
    }
    /// <summary>
    /// Should return a filled model thats contains "No model" in name.
    /// </summary>
    [Fact]
    public void ListFilteredModelByName()
    {
        string nameCheck = "No model";
        ModelController modelController = new ModelController();
        ICollection<VolvoModel> result = modelController.Get(true, 1, 1, nameCheck);

        if (result.Count.Equals(0)) 
            result = modelController.Get(false, 1, 1, nameCheck);

        Assert.Collection(
            result
        , collection =>
        {
            Assert.NotNull(collection.Name);
            Assert.Contains(nameCheck, collection.Name);
        });
    }
}
