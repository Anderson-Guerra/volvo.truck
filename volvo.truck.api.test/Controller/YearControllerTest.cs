﻿using System.Collections.Generic;
using System.Linq;
using volvo.truck.api.Controllers.Definitions;
using volvo.truck.api.Model.Definitions;
using Xunit;

namespace Controller;

public class YearControllerTest
{
    /// <summary>
    /// List of model year's.
    /// </summary>
    [Fact]
    public void ListYearModel()
    {
        ICollection<ModelYear> result = new YearController().GetModel();

        Assert.NotEmpty(result);
        Assert.InRange(result.Count(), 1, int.MaxValue);
    }

    /// <summary>
    /// List of filtered model year's.
    /// </summary>
    [Fact]
    public void ListFilteredYearModel()
    {
        ICollection<ModelYear> result = new YearController().GetModel(1, 1);

        Assert.Collection(
            result
        , collection =>
        {
            Assert.True(collection.Year > 1980);
        }
      );
    }

    /// <summary>
    /// List of Manufacturing year's.
    /// </summary>
    [Fact]
    public void ListYearManufacturing()
    {
        ICollection<ManufacturingYear> result = new YearController().GetManufacturing();

        Assert.NotEmpty(result);
        Assert.InRange(result.Count(), 1, int.MaxValue);
    }

    /// <summary>
    /// List of filtered Manufacturing year's.
    /// </summary>
    [Fact]
    public void ListFilteredYearManufacturing()
    {
        ICollection<ManufacturingYear> result = new YearController().GetManufacturing(1, 1);

        Assert.Collection(
            result
        , collection =>
        {
            Assert.True(collection.Year > 1980);
        }
      );
    }
}
