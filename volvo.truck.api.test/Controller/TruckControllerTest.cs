﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using volvo.truck.api.Controllers.Vehicles;
using volvo.truck.api.Model.Definitions;
using volvo.truck.api.Model.Vehicles;
using Xunit;

namespace Controller;

public class TruckControllerTest
{
    /// <summary>
    /// Truck name for test
    /// </summary>
    static string NameTruck
    {
        get
        {
            return __Clear.Database.NameTruck;
        }
    }

    /// <summary>
    /// Should return full list of trucks.
    /// </summary>
    [Fact]
    public void ListAllTruck()
    {
        ICollection<Truck> result = new TruckController().Get();

        Assert.NotEmpty(result);
        Assert.InRange(result.Count, 1, int.MaxValue);

        Assert.True(result.First().Enabled);
        Assert.True(
            result.First(first => first.Enabled.Equals(true)).ID 
            >= result.Last(last => last.Enabled.Equals(true)).ID
        );

        Assert.False(result.Last().Enabled);
        Assert.True(
            result.First(first => first.Enabled.Equals(false)).ID 
            >= result.Last(last => last.Enabled.Equals(false)).ID
        );
    }

    /// <summary>
    /// Should return list of enabled trucks.
    /// </summary>
    [Fact]
    public void ListEnabledTruck()
    {
        ICollection<Truck> result = new TruckController().GetEnabled();

        Assert.NotEmpty(result);
        Assert.InRange(result.Count, 1, int.MaxValue);
        Assert.DoesNotContain(result, any => any.Enabled.Equals(false));        
    }

    /// <summary>
    /// Should return list of disabled trucks.
    /// </summary>
    [Fact]
    public void ListDisabledTruck()
    {
        ICollection<Truck> result = new TruckController().GetDisabled();

        Assert.NotEmpty(result);
        Assert.InRange(result.Count, 1, int.MaxValue);
        Assert.DoesNotContain(result, any => any.Enabled.Equals(true));
    }

    /// <summary>
    /// Should return 2 filled trucks.
    /// </summary>
    [Fact]
    public void ListFilteredTruck()
    {
        ICollection<Truck> result = new TruckController().Get(true, 1, 2, "*");

        Assert.Collection(
            result
        , collection =>
        {            
            Assert.True(collection.Enabled);
            Assert.NotNull(collection.Name);
            Assert.NotNull(collection.VolvoModel);
            Assert.NotNull(collection.ModelYear);
            Assert.NotNull(collection.ManufacturingYear);
        }
        , collection =>
        {
            Assert.True(collection.Enabled);
            Assert.NotNull(collection.Name);
            Assert.NotNull(collection.VolvoModel);
            Assert.NotNull(collection.ModelYear);
            Assert.NotNull(collection.ManufacturingYear);
        });
    }

    /// <summary>
    /// Should return a filled trucks thats contains "Example" in name.
    /// </summary>
    [Fact]
    public void ListFilteredTruckByName()
    {
        string nameCheck = "Example";
        TruckController truckController = new();
        ICollection<Truck> result = truckController.Get(true, 1, 1, nameCheck);

        if (result.Count.Equals(0))
            result = truckController.Get(false, 1, 1, nameCheck);

        Assert.Collection(
            result
        , collection =>
        {
            Assert.Contains(nameCheck, collection.Name);
            Assert.NotNull(collection.Name);
            Assert.NotNull(collection.VolvoModel);
            Assert.NotNull(collection.ModelYear);
            Assert.NotNull(collection.ManufacturingYear);
        });
    }

    /// <summary>
    /// Should return truck by id.
    /// </summary>
    [Fact]
    public void GetTruck()
    {
        long idItem = 1;
        Truck result = new TruckController().Get(idItem);

        Assert.NotNull(result);
        Assert.True(result.ID.Equals(idItem));        
    }

    /// <summary>
    /// Create new truck.
    /// </summary>
    [Fact]
    public void CreateTruck()
    {
        TruckController truckController = new();
        Truck tTruck = truckController.Post(new Truck
        {
            Name = NameTruck
        ,   Enabled = true
        ,   VolvoModel = new VolvoModel { ID = 1 }
        ,   ModelYear = new ModelYear { ID = 1 }    
        ,   ManufacturingYear = new ManufacturingYear { ID = 1 }
        });

        Assert.True(tTruck.ID > 0);
        Assert.True(tTruck.ManufacturingYear.Year <= tTruck.ModelYear.Year);
    }

    /// <summary>
    /// Update teste truck.
    /// </summary>
    [Fact]
    public void UpdateTruck()
    {
        TruckController truckController = new();

        Truck? result = truckController.Get(false, 1, 1, NameTruck).FirstOrDefault();
        Assert.NotNull(result);

        if (result is not null && !result.ID.Equals(0))
        {
            result.Name = string.Concat(NameTruck, " update-", Guid.NewGuid());
            result.Enabled = true;
            result.VolvoModel = new VolvoModel { ID = 1 };
            result.ModelYear = new ModelYear { ID = 2 };
            result.ManufacturingYear = new ManufacturingYear { ID = 1 };
            Truck tTruck = truckController.Put(result);

            Assert.True(tTruck.Enabled);
            Assert.True(tTruck.VolvoModel.ID.Equals(1));
        }
    }

    /// <summary>
    /// Update teste truck with error.
    /// </summary>
    [Fact]
    public void UpdateTruckErrorModelYear()
    {
        TruckController truckController = new();

        Truck? result = truckController.Get(false, 1, 1, NameTruck).FirstOrDefault();
        Assert.NotNull(result);

        if (result is not null && !result.ID.Equals(0))
        {
            result.Name = string.Concat(NameTruck, " update-", Guid.NewGuid());
            result.Enabled = true;
            result.VolvoModel = new VolvoModel { ID = 1 };
            result.ModelYear = new ModelYear { ID = 3 };
            result.ManufacturingYear = new ManufacturingYear { ID = 1 };

            try
            {
                Truck tTruck = truckController.Put(result);
            } 
            catch (Exception e)
            {
                Assert.Equal("Model year is only allowed one year in advance from manufacture date!", e.Message);
            }
        }
    }

    /// <summary>
    /// Update teste truck with error.
    /// </summary>
    [Fact]
    public void UpdateTruckErrorManufacturingYear()
    {
        TruckController truckController = new();

        Truck? result = truckController.Get(false, 1, 1, NameTruck).FirstOrDefault();
        Assert.NotNull(result);

        if (result is not null && !result.ID.Equals(0))
        {
            result.Name = string.Concat(NameTruck, " update-", Guid.NewGuid());
            result.Enabled = true;
            result.VolvoModel = new VolvoModel { ID = 1 };
            result.ModelYear = new ModelYear { ID = 1 };
            result.ManufacturingYear = new ManufacturingYear { ID = 2 };

            try
            {
                Truck tTruck = truckController.Put(result);
            }
            catch (Exception e)
            {
                Assert.Equal("Manufacturing Year need to be lower or equal model year!", e.Message);
            }
        }
    }

    /// <summary>
    /// Disable Test added truck's.
    /// </summary>
    [Fact]
    public void DisableTruck()
    {
        TruckController truckController = new();
        ICollection<Truck> result = truckController.Get(true, 1, 1000, NameTruck);

        Assert.InRange(result.Count, 1, 1000);

        for (int i = 0; i < result.Count; i++)
        {
            OkResult tOkResult = truckController.Delete(result.ElementAt(i).ID);
            Assert.True(tOkResult.StatusCode.Equals(200));
        }     
    }
}
