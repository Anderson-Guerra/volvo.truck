﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using volvo.truck.api.Data;
using volvo.truck.api.Model.Vehicles;
using Xunit;

namespace _Data {
    public class Database
    {
        /// <summary>
        /// Migrate DataBase.
        /// </summary>
        [Fact]
        public void CheckConnection()
        {
            new LocalDBContext().Database.Migrate();
            Assert.True(Connection.CheckLocalDB());
        }
    }
}

namespace __Clear
{
    public class Database
    {
        /// <summary>
        /// Truck name for test
        /// </summary>
        internal static string NameTruck
        {
            get { return "xUnit Truck test"; }
        }

        /// <summary>
        /// Clean DataBase.
        /// </summary>
        [Fact]
        public void ClearTeste()
        {
            if (Debugger.IsAttached)
            {
                LocalDBContext dbContext = new LocalDBContext();

                ICollection<Truck> result = dbContext.Trucks
                    .Where(where => (where.Name ?? string.Empty).Contains(Database.NameTruck)).ToList();

                dbContext.RemoveRange(result);
                dbContext.SaveChanges();

                result = dbContext.Trucks
                    .Where(where => (where.Name ?? string.Empty).Contains(Database.NameTruck)).ToList();

                Assert.True(result.Count.Equals(0));
            }
        }
    }
}