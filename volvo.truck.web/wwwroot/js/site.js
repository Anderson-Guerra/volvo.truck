﻿(function (w, d, $) {    
    w._lastItem = 0;
    w._call = {
        apiUrl: w.apiUrl,
        truckEndPoint: w.apiUrl + 'truck/',
        truckModelPoint: w.apiUrl + 'model/',
        truckYearPoint: w.apiUrl + 'year/',
        get: function (url, callback, error) {
            $.ajax({
                type: 'GET',
                url: url,
                dataType: 'json',
                success: 'function' === typeof (callback)
                    ? function (data) {
                        callback($(data.d||data));
                    }: function () {
                        console.warn('No callback function set!')
                    },
                error: error || function(req, status, error) {
                    console.error(status);
                }
            });
        },
        post: function (url, data, callback, error) {
            $.ajax({
                type: 'POST',
                url: url,
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                encode: true,
                success: 'function' === typeof (callback)
                    ? function (data) {
                        callback($(data.d||data));
                    } : function () {
                        console.warn('No callback function set!')
                    },
                error: error || function(req, status, error) {
                    console.error(status);
                }
            });
        },
        put: function (url, data, callback, error) {
            $.ajax({
                type: 'PUT',
                url: url,
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                encode: true,
                success: 'function' === typeof (callback)
                    ? function (data) {
                        callback($(data.d||data));
                    } : function () {
                        console.warn('No callback function set!')
                    },
                error: error || function (req, status, error) {
                    console.error(status);
                }
            });
        },
        delete: function (url, callback, error) {
            $.ajax({
                type: 'DELETE',
                url: url,
                dataType: 'json',
                success: 'function' === typeof (callback)
                    ? function (data) {
                        callback($(data.d||data));
                    } : function () {
                        console.warn('No callback function set!')
                    },
                error: error || function (req, status, error) {
                    console.error(status);
                }
            });
        }        
    };
    w._page = {
        form: {
            model: '#fmodel',
            modelyear: '#fmodelyear',
            manufatureyear: '#fmanufatureyear'
        },
        loadSelect: function () {
            return _page
                .loadModel()
                .loadModelYear()
                .loadManufatureYear();
        },
        loadModel: function () {
            _call.get(
                _call.truckModelPoint + 'get',
                function (data) {
                    data.sort(_page.sortbyName);
                    let fActive = $(_page.form.model);

                    data.each(function () {
                        if (this.enabled)
                            $('<option>')
                                .val(this.id)
                                .html(this.name)
                                .appendTo(fActive);
                    });
                });

            return _page;
        },
        loadModelYear: function () {
            _call.get(
                _call.truckYearPoint + 'get-model',
                function (data) {
                    data.sort(_page.sortbyYear);
                    let fActive = $(_page.form.modelyear);

                    data.each(function () {
                        $('<option>')
                            .val(this.id)
                            .html(this.year)
                            .appendTo(fActive);
                    });
                });

            return _page;
        },
        loadManufatureYear: function () {
            _call.get(
                _call.truckYearPoint + 'get-manufacturing',
                function (data) {
                    data.sort(_page.sortbyYear);
                    let fActive = $(_page.form.manufatureyear);

                    data.each(function () {
                        $('<option>')
                            .val(this.id)
                            .html(this.year)
                            .appendTo(fActive);
                    });
                });

            return _page;
        },        
        sortbyName: function (pLeft, pRight) {
            if (pLeft.name > pRight.name) return 1;
            if (pLeft.name < pRight.name) return -1;
            return 0;
        },
        sortbyYear: function (pLeft, pRight) {
            if (pLeft.year > pRight.year) return -1;
            if (pLeft.year < pRight.year) return 1;
            return 0;
        }        
    };
    w._truck = {
        form: {
            name: '#fname',
            close: '#fclose, .fclose',
            send: '#fsend, .fsend',
            edit: '.fedit',
            disable: '.fdisable'
        },
        validate: function (name, model, modelyear, manufacturingyear) {
            if (name.val().length < 3) {
                alert('Name need to be 3 characters long!');
                return false;
            }
            if (model.val() === '0') {
                alert('Model selected is invalid!')
                return false;
            }
            if (modelyear.val() === '0') {
                alert('Model year selected is invalid!')
                return false;
            }
            if (manufacturingyear.val() === '0') {
                alert('Manufacturing Year selected is invalid!')
                return false;
            }
            if (Number(manufacturingyear.val() > Number(modelyear.val()))) {
                alert('Manufacturing year need to be lower or equal model year!');
                return false;
            }
            if (Number(modelyear.val() > (Number(manufacturingyear.val()) + 1))) {
                alert('Model year is only allowed one year in advance from manufacture date!');
                return false;
            }
            return true;
        },
        bindForm: function (grid) {
            $(_truck.form.close)
                .unbind('click.fbind')
                .bind('click.fbind', function () {
                    let inputs = { ..._page.form, ..._truck.form };
                    for (let prop in inputs) {
                        let eActive = $(inputs[prop]);

                        if (eActive.prop('nodeName') === 'INPUT') eActive.val('');
                        if (eActive.prop('nodeName') === 'SELECT') eActive.val(0);
                    }

                    $(_truck.form.send)
                        .unbind('click.fbind')
                        .bind('click.fbind', function () {
                            let name = $(_truck.form.name);
                            let model = $(_page.form.model);
                            let modelyear = $(_page.form.modelyear);
                            let manufacturingyear = $(_page.form.manufatureyear);

                            if (_truck.validate(name, model, modelyear, manufacturingyear))
                                _call.post(
                                    _call.truckEndPoint + 'post',
                                    {
                                        "name": name.val(),
                                        "volvoModel": {
                                            "id": model.val(),
                                            "name": "",
                                            "enabled": true
                                        },
                                        "modelYear": {
                                            "id": modelyear.val(),
                                            "year": 0
                                        },
                                        "manufacturingYear": {
                                            "id": manufacturingyear.val(),
                                            "year": 0
                                        },
                                        "enabled": true
                                    },
                                    function (data) {
                                        data.each(function (i) { grid.prepend(_truck.table.main(data[i], 'NEW')); });
                                        _truck.closeForm();
                                        _truck.bindForm(grid);
                                    });
                        });
                });

            $(_truck.form.edit)
                .unbind('click.fbind')
                .bind('click.fbind', function () {
                    let component = $(this);
                    let cItem = component.data('item');                    

                    if (!isNaN(cItem) && cItem > 0)
                        _call.get(
                            _call.truckEndPoint + 'get/' + cItem,
                            function (data) {
                                let enabled = true;
                                if (data.length === 1) {
                                    let current = data[0];
                                    $(_truck.form.name).val(current.name);
                                    $(_page.form.model).val(current.volvoModel.id);
                                    $(_page.form.modelyear).val(current.modelYear.id);
                                    $(_page.form.manufatureyear).val(current.manufacturingYear.id);
                                    enabled = current.enabled;
                                }

                                $(_truck.form.send)
                                    .unbind('click.fbind')
                                    .bind('click.fbind', function () {
                                        let name = $(_truck.form.name);
                                        let model = $(_page.form.model);
                                        let modelyear = $(_page.form.modelyear);
                                        let manufacturingyear = $(_page.form.manufatureyear);

                                        if (_truck.validate(name, model, modelyear, manufacturingyear))
                                            _call.put(
                                                _call.truckEndPoint + 'put',
                                                {
                                                    "id": cItem,
                                                    "name": name.val(),
                                                    "volvoModel": {
                                                        "id": model.val(),
                                                        "name": "",
                                                        "enabled": true
                                                    },
                                                    "modelYear": {
                                                        "id": modelyear.val(),
                                                        "year": 0
                                                    },
                                                    "manufacturingYear": {
                                                        "id": manufacturingyear.val(),
                                                        "year": 0
                                                    },
                                                    "enabled": enabled
                                                },
                                                function (data) {
                                                    data.each(function (i) {
                                                        $('#row-id-' + cItem).replaceWith(_truck.table.main(data[i], 'UPDATED'));
                                                        w._lastItem = cItem;
                                                    });

                                                    _truck.closeForm();
                                                    _truck.bindForm(grid);
                                                });
                                    });
                            }
                        );
                });

            $(_truck.form.disable)
                .unbind('click.fbind')
                .bind('click.fbind', function () {
                    let component = $(this);
                    let cItem = component.data('item');
                    let cRow = component.data('row');

                    if (!isNaN(cItem) && cItem > 0)
                        _call.get(
                            _call.truckEndPoint + 'get/' + cItem,
                            function (data) {
                                if (data.length === 1) {
                                    let current = data[0];
                                    current.enabled = !current.enabled;

                                    _call.put(
                                        _call.truckEndPoint + 'put',
                                        current,
                                        function (data) {
                                            data.each(function (i) {
                                                $('#row-id-' + cItem).replaceWith(_truck.table.main(data[i], cRow));
                                                w._lastItem = 0;
                                            });

                                            _truck.bindForm(grid);
                                        });
                                }
                            }
                        );
                });

            if (Number(w._lastItem) > 0)
                window.location.hash = '#row-id-' + w._lastItem;

            return _page;
        },
        closeForm: function () {
            let bClose = $(_truck.form.close);
            bClose.trigger('click');
            bClose.click();
        },
        table: {
            main: function (current, extra) {
                return $('<tr>')
                    .attr('id', 'row-id-' + current.id)
                    .append($('<th>')
                        .attr({ 'scope': 'row' })
                        .html(extra || ''))
                    .append($('<td>')
                        .html(current.id))
                    .append($('<td>')
                        .html(current.name))
                    .append($('<td>')
                        .html(current.volvoModel.name))
                    .append($('<td>')
                        .html(current.modelYear.year))
                    .append($('<td>')
                        .html(current.manufacturingYear.year))
                    .append($('<td>')
                        .html(current.enabled ? 'Yes' : 'No'))
                    .append($('<td>')
                        .append($('<button>')
                            .addClass(['btn', 'btn-outline-primary', 'fedit'])
                            .html('Edit')
                            .attr({
                                'data-bs-toggle': 'modal',
                                'data-bs-target': '#ChangeTruck',
                                'data-row': extra,
                                'data-item': current.id
                            })))
                    .append($('<td>')
                        .append($('<button>')
                            .addClass(['btn', 'fdisable'])
                            .addClass(current.enabled ? 'btn-outline-danger' : 'btn-outline-success')
                            .html(current.enabled ? 'Disable' : 'Enable')
                            .attr({
                                'data-row': extra,
                                'data-item': current.id
                            })))
            }
        }
    };    
})(window, document, jQuery);
